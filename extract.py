import openpyxl 
import pandas as pd
import sqlite3

#"https://www.francecompetences.fr/app/uploads/2024/01/Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx" 
# wb = openpyxl.load_workbook('Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx')
# #print(wb.sheetnames)   # Output: ['Me lire', 'Onglet 2 - global', 'Onglet 3 - référentiel NPEC', 'Onglet 4 - CPNE-IDCC']

# ws2 = wb['Onglet 2 - global']
# print('Total number of rows: '+str(ws2.max_row)+'. And total number of columns: '+str(ws2.max_column))
# # Total number of rows: 3818. And total number of columns: 217

# ws3 = wb['Onglet 3 - référentiel NPEC']
# print('Total number of rows: '+str(ws3.max_row)+'. And total number of columns: '+str(ws3.max_column))
# # Total number of rows: 812173. And total number of columns: 9

# ws4 = wb['Onglet 4 - CPNE-IDCC']
# print('Total number of rows: '+str(ws4.max_row)+'. And total number of columns: '+str(ws4.max_column))
# # Total number of rows: 437. And total number of columns: 3

filename= 'Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx'
#filename= "excel_test.xlsx"
df3 = pd.read_excel(filename, sheet_name='Onglet 3 - référentiel NPEC',skiprows=3)
df4 = pd.read_excel(filename, sheet_name='Onglet 4 - CPNE-IDCC', skiprows=2)

#print(df3.columns) # ['Code RNCP', 'Libellé de la formation',
    #    'Certificateur* \n(*liste non exhaustive - premier dans la fiche RNCP)',
    #    'Libellé du Diplôme', 'Code CPNE', 'CPNE', 'NPEC final', 'Statut',
    #    'Date d'applicabilité des NPEC** \n(**hors contrats couverts par la valeur d'amorçage + voir onglet "Me lire")']
#print(df4.columns)  # ['Code CPNE', 'CPNE', 'IDCC']
# Rename columns:
df3.columns = ["Code RNCP", "Libelle formation", "Certificateur", "Diplome",'Code CPNE', 'CPNE', 'NPEC final', 'Statut', 'Date']

# Delete row df3["Libelle formation"]=="Agent de bio-nettoyage en établissements de soin ou d'hébergement"), rows count from 3814 to 3813
df3_Norm=df3.drop(df3[df3["Libelle formation"]=="Agent de bio-nettoyage en établissements de soin ou d'hébergement"].index)
# Datatype date / datetime not suported in sqlite, changed to text
df3_Norm['Date'] = df3_Norm['Date'].astype(str)

# Creation of DF specific for each table in database
df_RNCP = df3_Norm[["Code RNCP", "Libelle formation", "Certificateur", "Diplome"]].drop_duplicates()
#print(df_RNCP.loc[df_RNCP['Code RNCP'] == 'RNCP31931/RNCP36511/RNCP37609', 'Libelle formation'].value_counts())
df_CPNE = df3_Norm[['Code CPNE', 'CPNE']].drop_duplicates()
df_CPNE_RNCP = df3_Norm[["Code RNCP",'Code CPNE','NPEC final', 'Statut', 'Date']].drop_duplicates()
df_IDCC=df4[['Code CPNE', 'IDCC']]

# print(df_CPNE_RNCP.info())
# print(df_CPNE_RNCP.head())

######################## INSERTS INTO SQLITE DB #########""

connection = sqlite3.connect("db_NPEC.db")

cursor = connection.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS CPNE (code integer PRIMARY KEY, nom TEXT)")
cursor.execute("CREATE TABLE IF NOT EXISTS IDCC (code_CPNE INTEGER, code_IDCC INTEGER PRIMARY KEY, FOREIGN KEY (code_CPNE) REFERENCES CPNE(code))")
cursor.execute("CREATE TABLE IF NOT EXISTS RNCP (code text PRIMARY KEY, certification text, certificateur text, libelle_titre text)")
cursor.execute("CREATE TABLE IF NOT EXISTS CPNE_RNCP (code_RNCP text, code_CPNE INTEGER, NPEC integer, statut TEXT CHECK (statut IN ('A', 'R', 'CPNE')), date_NPEC text, FOREIGN KEY (code_RNCP) REFERENCES RNCP(code),FOREIGN KEY (code_CPNE) REFERENCES CPNE(code))")

# Convert DataFrame to list of tuples
data_CPNE= df_CPNE.values.tolist()
data_IDCC = df_IDCC.values.tolist()
data_CPNE_RNCP = df_CPNE_RNCP.values.tolist()
data_RNCP =df_RNCP.values.tolist()
# Insert data into the table
cursor.executemany("INSERT INTO CPNE VALUES (?,?)", data_CPNE)
connection.commit()

cursor.executemany("INSERT INTO IDCC VALUES (?, ?)", data_IDCC )
connection.commit()

cursor.executemany("INSERT INTO RNCP VALUES (?,?, ?, ?)", data_RNCP)
connection.commit()

cursor.executemany("INSERT INTO CPNE_RNCP VALUES (?,?,?,?,?)", data_CPNE_RNCP)
connection.commit()

# Select and print the first 10 rows from the table to verify the insertion
cursor.execute("SELECT * FROM CPNE_RNCP LIMIT 10")
rows = cursor.fetchall()
for row in rows:
    print(row)


connection.close()