# EXO read Excel - Extract into sqlite DB - Create API to interact
Source excel file:
[link to web](https://www.francecompetences.fr/base-documentaire/referentiels-et-bases-de-donnees/)
download from "Référentiels" file 31 janvier 2023

href="https://www.francecompetences.fr/app/uploads/2024/01/Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx" 

As it is a heavy file that takes too much memory to read, we need to search through it "blindly"
Which are the sheet names, content, etc...
```python
import openpyxl 

wb = openpyxl.load_workbook('Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx')
print(wb.sheetnames)
```
## Analysis of excel file content
CPNE= Commission paritaire nationale emploi et formation professionnelle (CPNEFP ou CPNEF ou CPNE)
IDCC= Identifiant Des Conventions Collectives
NPEC= Niveau de Prise En Charge du contrat d'apprentissage
RNCP= Répertoire national des certifications professionnelles



https://www.freecodecamp.org/news/how-to-create-read-update-and-search-through-excel-files-using-python-c70680d811d4/
https://www.datacamp.com/tutorial/python-excel-tutorial


Sqlite data tpyes:
Column Datatype	    Types Allowed In That Column
INTEGER	    ->      INTEGER, REAL, TEXT, BLOB
REAL	    ->      REAL, TEXT, BLOB
TEXT	    ->      TEXT, BLOB
BLOB	    ->      INTEGER, REAL, TEXT, BLOB 
