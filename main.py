from fastapi import FastAPI, UploadFile, File, Form
import sqlite3


DATABASE_URL = "sqlite:///db_NPEC.db"

#uvicorn main:app --reload
app = FastAPI(debug=True)

# connection = sqlite3.connect("db_NPEC.db")
# cursor = connection.cursor()

def get_connection():
    return sqlite3.connect("db_NPEC.db")

# @app.get('/')
# def home():
#     return "Feliz dia de la mujer"


@app.get("/CPNE/{code}")
def get_CPNE(code: int):
    connection = get_connection()
    cursor = connection.cursor()
    cursor.execute("SELECT code, nom FROM CPNE WHERE code = ?", (code,))
    result = cursor.fetchall()
    connection.close()
    return result

# @app.get("/CPNE/{code}")
# def get_CPNE(code: int) ->dict:
#     connection = get_connection()
#     cursor = connection.cursor(dictionary=True)
#     cursor.execute("SELECT code, nom FROM CPNE WHERE code = ?", (code,))
#     result = cursor.fetchall()
#     connection.close()
#     return result
    
@app.post("/RNCP_CPNE/{search_word}")
def search_keyword(keyword):
    connection = get_connection()
    cursor = connection.cursor()
    cursor.execute("""
                   SELECT * FROM CPNE_RNCP
                   JOIN RNCP ON CPNE_RNCP.code_RNCP= RNCP.code
                   JOIN CPNE ON CPNE_RNCP.code_CPNE= CPNE.code
                   WHERE certification = "Préparateur en pharmacie";
                   """
                   , (search_word,))
    result = cursor.fetchall()


# Routes:
# GET /table
# route qui donne la liste des colonnes des résultats en json.
# Exemple : [“nom”, “prenom”, “age”]

# POST /search
# route qui renvoie le résultat de la recherche.
# Body : { “query”: “ce que je cherche”}
# Exemple de réponse :

# [
#   {
#     "nom": "bob",
#     "prenom": "loblo"
#     "age": 12
#   },
#   {
#     "nom": "john",
#     "prenom": "wick"
#     "age": 54
#   }
# ]


# Cette route renvoie au maximum 100 résultats.
# La requête peut avoir un paramètre page chaque page représente 100 résultats.
# Par exemple si on demande la page 3, on à les résultat de 301 à 400
# Exemple de requête d’une page : /search?page=2



# if __name__ == '__main__':
#     print("main executed")